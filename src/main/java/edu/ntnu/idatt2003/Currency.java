package edu.ntnu.idatt2003;

/**
 * Class representing a currency.
 * Currencies are identified by their ISO 4217 currency codes.
 */
public interface Currency {

    /**
     * Get the ISO 4217 currency code of this currency
     * @return The ISO 4217 currency code for this currency
     */
    String getCode();

    /**
     * Get a name for this currency that is suitable for display
     * @return The display name for this currency
     */
    String getDisplayName();

    /**
     * Get the primary currency for a given ISO 3166-1 alpha-2 country code.
     * @param countryCode A ISO 3166-1 alpha-2 two-letter country code
     * @return A currency for the given country code
     * @throws UnknownCurrencyException If no currency exists for this code
     */
    static Currency of(String countryCode) throws UnknownCurrencyException {
        return switch (countryCode) {
            case "NO" -> new NorwegianCurrency();
            case "SE" -> new SwedishCurrency();
            case "DK" -> new DanishCurrency();
            default -> throw new UnknownCurrencyException("Unable to create a currency for country code '" + countryCode + "'");
        };
    }
}
