package edu.ntnu.idatt2003;

public class DanishCurrency implements Currency {

    @Override
    public String getCode() {
        return "DKK";
    }

    @Override
    public String getDisplayName() {
        return "Danish krone";
    }
}
