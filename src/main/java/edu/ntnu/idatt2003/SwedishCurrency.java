package edu.ntnu.idatt2003;

public class SwedishCurrency implements Currency {

    @Override
    public String getCode() {
        return "SEK";
    }

    @Override
    public String getDisplayName() {
        return "Swedish krona";
    }
}
