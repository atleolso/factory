package edu.ntnu.idatt2003;

import java.util.Locale;

public class CurrencyFactory {

    /**
     * Get the primary currency for a given ISO 3166-1 alpha-2 country code.
     * @param countryCode A ISO 3166-1 alpha-2 two-letter country code
     * @return A currency for the given country code
     * @throws UnknownCurrencyException If no currency exists for this code
     */
    public static Currency get(String countryCode) throws UnknownCurrencyException {
        return switch (countryCode) {
            case "NO" -> new NorwegianCurrency();
            case "SE" -> new SwedishCurrency();
            case "DK" -> new DanishCurrency();
            default -> throw new UnknownCurrencyException("Unable to create a currency for country code '" + countryCode + "'");
        };
    }

    /**
     * Get the primary currency for a given locale.
     * @param locale A locale representing a specific geographical, political, or cultural region
     * @return A currency for the given locale
     * @throws UnknownCurrencyException If no currency exists for this locale
     */
    public static Currency get(Locale locale) throws UnknownCurrencyException {
        return get(locale.getCountry());
    }
}
