package edu.ntnu.idatt2003;

public class UnknownCurrencyException extends Exception {

    public UnknownCurrencyException(String message) {
        super(message);
    }
}
