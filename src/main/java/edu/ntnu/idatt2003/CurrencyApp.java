package edu.ntnu.idatt2003;

public class CurrencyApp {

    public static void main(String... args) {
        Currency currency;
        String countryCode = "NO"; //sample country code

        // instead of this...
        try {
            currency = switch (countryCode) {
                case "NO" -> new NorwegianCurrency();
                case "SE" -> new SwedishCurrency();
                case "DK" -> new DanishCurrency();
                default -> throw new UnknownCurrencyException("Unable to create a currency for country code '" + countryCode + "'");
            };
            System.out.println("My currency is " + currency.getDisplayName());
        } catch (UnknownCurrencyException e) {
            System.err.println(e.getMessage());
        }

        // ...we can do this...
        try {
            currency = CurrencyFactory.get(countryCode);
            System.out.println("My currency is " + currency.getDisplayName());
        } catch (UnknownCurrencyException e) {
            System.err.println(e.getMessage());
        }

        // ...or this (using a static factory method in the Currency interface)
        // note: static factory method ≠ GoF factory method pattern
        try {
            currency = Currency.of(countryCode);
            System.out.println("My currency is " + currency.getDisplayName());
        } catch (UnknownCurrencyException e) {
            System.err.println(e.getMessage());
        }
    }
}
