package edu.ntnu.idatt2003;

public class NorwegianCurrency implements Currency {

    @Override
    public String getCode() {
        return "NOK";
    }

    @Override
    public String getDisplayName() {
        return "Norwegian krone";
    }
}
